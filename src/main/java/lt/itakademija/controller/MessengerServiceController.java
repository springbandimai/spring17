package lt.itakademija.controller;

import lt.itakademija.model.Id;
import lt.itakademija.model.command.CreateContact;
import lt.itakademija.model.command.CreateMessage;
import lt.itakademija.model.command.UpdateContact;
import lt.itakademija.model.query.Contact;
import lt.itakademija.model.query.Message;
import lt.itakademija.repository.MessengerRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by mariusg on 2017.03.19.
 */
@Controller
public class MessengerServiceController {

    private final MessengerRepository repository;

    public MessengerServiceController(MessengerRepository repository) {
        this.repository = repository;
    }

    @PostMapping(value = "/webapi/messenger/contacts")
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    Id createContact(@RequestBody CreateContact createContact) {
        //throw new UnsupportedOperationException("not implemented");
        return new Id(repository.createContact(createContact));
    }

    @GetMapping(value = "/webapi/messenger/contacts")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    List<Contact> getContacts() {
        //throw new UnsupportedOperationException("not implemented");
        return repository.getContacts();
    }

    @GetMapping(path = "/webapi/messenger/contacts/{contactId}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    Contact getContact(@PathVariable Long contactId) {
        //throw new UnsupportedOperationException("not implemented");
        return repository.getContact(contactId);
    }

    @PutMapping(path = "/webapi/messenger/contacts/{contactId}")
    @ResponseStatus(HttpStatus.OK)
    public void updateContact(@PathVariable Long contactId, @RequestBody UpdateContact updateContact) {
        //throw new UnsupportedOperationException("not implemented");
        repository.updateContact(contactId, updateContact);
    }

    @DeleteMapping(path = "/webapi/messenger/contacts/{contactId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteContact(@PathVariable Long contactId) {
        //throw new UnsupportedOperationException("not implemented");
        repository.deleteContact(contactId);
    }

    @PostMapping(path = "/webapi/messenger/messages/{contactId}")
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    Id createMessage(@PathVariable Long contactId,
                     CreateMessage createMessage) {
        //throw new UnsupportedOperationException("not implemented");
        return new Id(repository.createMessage(contactId, createMessage));
    }

    @GetMapping(path = "/webapi/messenger/messages/{contactId}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    List<Message> getMessages(@PathVariable Long contactId) {
        //throw new UnsupportedOperationException("not implemented");
        return repository.getMessages(contactId);
    }

}
